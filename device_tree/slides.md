---
title: Device Tree Overview
author: Lucas A. M. Magalhães
date: 2018-07-10
rights: © 2018 Lucas A. M. Magalhães, CC BY-SA
pandoc-latex-fontsize:
  - classes: [smallcontent]
    size: tiny
  - classes: [largecontent, important]
    size: huge
---

# Introduction

“No concentrated effort to have a framework for things… since we try to support a lot of the ARM architecture, it’s been a painful thing for me to see, look at the x86 tree and ARM tree and it’s many times bigger. It’s not constrained by this nice platform thing, it just has random crap all over it. And it was getting to me[…] I just snapped, and instead of running around naked with a chainsaw like I usually do, I started talking to people and a lot of people admitted it’s a problem.” - Linus Torvalds at Linuxcon 2011

# Introduction

* Created by the Open Firmware project.
* Originally used by PowerPC and Spark architectures.
* Mandatory for every new ARM Soc since 2012.

# Platform devices

On the driver side a structure must be declared. It will
be used by the kernel to know many information about the
driver and to match it with the platform device itself.

~~~~~
static struct platform_driver mxcuart_driver = {
    .driver = {
        .name = "mxcintuart",
    },
    .probe = mxcuart_probe,
    .remove = mxcuart_remove,
    [..,.]
};
~~~~~

# Platform devices

On the platform side the hardware must be declared.
Here the resource is declared, describes a memory
mapped uart for the  i.MX platform of Freescale.

~~~~~
static struct resource mxc_uart_resources1[] = {
        {
                .start = UART1_BASE_ADDR,
                .end = UART1_BASE_ADDR + 0x0B5,
                .flags = IORESOURCE_MEM,
        },
        {
                .start = MXC_INT_UART1,
                .flags = IORESOURCE_IRQ,
        },
};
~~~~~

# Platform devices

The resource is used in the device declaration.
Attention to the *.name* property which matches with
the driver name used o it's declaration.

~~~~~
static struct platform_device mxc_uart_device1 = {
        .name = "mxcintuart",
        .id = 0,
        .num_resources = ARRAY_SIZE(mxc_uart_resources1),
        .resource = mxc_uart_resources1,
        .dev = {
                .platform_data = &mxc_ports[0],
                },
};
~~~~~

# Platform devices

During the driver initialization it should register as
a platform driver.

~~~~~
static int __init mxcuart_init(void)
{
        [...]
        ret = platform_driver_register(&mxcuart_driver);
        [...]
},
~~~~~

The platform durin initialization should register the
device.

~~~~~
static int __init mxc_init_uart(void)
{
        [...]
        platform_device_register(&mxc_uart_device1);
        [...]
}
~~~~~

# Platform devices

The platforms are under the arch tree and describes
the many different hardware. They stay hardcoded so
a kernel compiled for this platform devices does not
work on many different device even they have the same
architecture.

The ARM architecture had a initial solution for this
but the platform specifications still hardcoded.

# Device Tree

The device tree is a way to describe a whole SoC so
the kernel can start the correct platform devices
with the correct parameters without hardcoding the
platform.

So a kernel compiled for ARM architecture can run in
many different SoCs.

# How it works

1. The hardware is described in a device tree structure.
2. It is compiled as a blob and is written in a non volatile
    memory known by the bootloader
3. During boot the bootloader passes a pointer to the device
    tree to the kernel.
4. The kernel then instantiates the drivers needed by the device
    tree and passes the specific information to them.

# Syntax

~~~~~
#include "some-other-dtc"

{
    node@addr {
        property = "value";
        other-property = <1>;
        reference-to-something = <&node1>;
        list-of-bytes = [0x01 0x02 0x03];

        child@addr {
            [...]
        };
    };
};
~~~~~

# Example

~~~~~smallcontent
{
  model = "TI OMAP3 BeagleBoard xM";
  compatible = "ti,omap3-beagle-xm, ti,omap3-beagle", "ti,omap3";

  memory {
          device_type = "memory";
          reg = <0x80000000 0x20000000>; /* 512 MB */
  };

  leds {
          compatible = "gpio-leds";
          pmu_stat {
                  label = "beagleboard::pmu_stat";
                  gpios = <&twl_gpio 19 0>; /* LEDB */
          };

          heartbeat {
                  label = "beagleboard::usr0";
                  gpios = <&gpio5 22 0>; /* 150 -> D6 LED */
                  linux,default-trigger = "heartbeat";
          };

          mmc {
                  label = "beagleboard::usr1";
                  gpios = <&gpio5 21 0>; /* 149 -> D7 LED */
                  linux,default-trigger = "mmc0";
          };
  };

};
~~~~~

# The driver side

To access the device tree information from the
driver just use the platform api. For exaple:

~~~
r = platform_get_resource(pdev, IORESOURCE_MEM, 0);
~~~

It is possible to get custom properties values using
the *of_get_property* function.

~~~
if (of_get_property(node, "wakeup-source", NULL))
~~~

# Compiling

* The device trees are described in *.dts* files
* Include files are described in *.dtsi* files

The *Device Tree Compiler* is used to create the
*Device Tree Blob* that will be loaded on the
SoC memory.

~~~~~
dtc -O dtb -o p4080ds.dtb p4080ds.dts
~~~~~

# Bibliography

* <https://lwn.net/articles/572692/>
* <https://lwn.net/articles/448502/>
* <https://events.static.linuxfound.org/sites/events/files/slides/petazzoni-device-tree-dummies.pdf>
* <https://www.kernel.org/doc/Documentation/devicetree/usage-model.txt>
* <https://sergioprado.org/linux-e-o-suporte-a-device-tree-parte-1>

# License

This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
