#define container_of(ptr, type, member) (                \
  void *__mptr = (void *)(ptr);                          \
  BUILD_BUG_ON_MSG(!__same_type(*(ptr),                  \
                                ((type *)0)->member) &&  \
  !__same_type(*(ptr), void),                            \
  "pointer type mismatch in container_of()");            \
  ((type *)(__mptr - offsetof(type, member))); })

